// Jackson Kwan 1938023
package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] bookArr = new Book[5];
		bookArr[0] = new Book("Harry Potter", "J.K. Rowling");
		bookArr[2] = new Book("Hamlet", "William Shakespeare");
		bookArr[1] = new ElectronicBook("Macbeth", "William Shakespeare", 30);
		bookArr[3] = new ElectronicBook("To kill a MockingBird", "Harper Lee", 100);
		bookArr[4] = new ElectronicBook("Death of a Salesman", "Arthur Miller", 150);
		
		for(int i=0; i<bookArr.length; i++) {
			System.out.println(bookArr[i].toString());
		}
	}
}
