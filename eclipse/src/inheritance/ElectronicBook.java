//Jackson Kwan 1938023
package inheritance;

public class ElectronicBook extends Book {
	private int numberBytes;
	
	public ElectronicBook(String booktitle, String authorname, int numberBytes) {
		super(booktitle, authorname);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
		String fromBase = super.toString();
		return(fromBase + "Number of Bytes: " + numberBytes);
	}
}
