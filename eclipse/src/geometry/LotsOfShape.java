// Jackson Kwan 1938023
package geometry;

public class LotsOfShape {
	public static void main (String[] args) {
		Shape[] shapeArr = new Shape[5];
		shapeArr[0] = new Rectangle(3.5, 4);
		shapeArr[1] = new Rectangle(3.6, 2);
		shapeArr[2] = new Circle(4.7);
		shapeArr[3] = new Circle(7);
		shapeArr[4] = new Square(10);
		
		for(int i=0; i<shapeArr.length;i++) {
			System.out.println(shapeArr[i].getArea());
			System.out.println(shapeArr[i].getPerimeter());
		}
	}
}
